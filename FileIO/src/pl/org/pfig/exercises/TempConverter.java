package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	private final String path = "resources/";
	
	public double[] readTemp(String filename) {
		double[] ret = new double[countLines(filename)];
		
		File f = new File(path + filename);
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
			int i = 0;
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				ret[i++] = Double.parseDouble(currentLine.replace(",", "."));
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}		
		
		return ret;
	}
	
	private int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
	
	public double toKelvin(double c) {
		return c + 273.15;
	}
	
	public double toFahrenheit(double c) {
		return (c * 1.8) + 32;
	}
	
	public void writeTemp(double[] temp) {
		
		try {
			FileOutputStream fosK = new FileOutputStream(path + "tempK.txt");
			FileOutputStream fosF = new FileOutputStream(path + "tempF.txt");
			
			PrintWriter pwK = new PrintWriter(fosK);
			PrintWriter pwF = new PrintWriter(fosF);
			
			for(double c : temp) {
				pwK.println(toKelvin(c));
				pwF.println(toFahrenheit(c));
			}
			
			pwK.close();
			pwF.close();
			
		} catch(FileNotFoundException e) { 
			System.out.println(e.getMessage());
		}
	}
}
