package pl.org.pfig.exercises;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {
	private final String path = "resources/";
	private String filename;
	
	public AvgChecker(String filename) {
		this.filename = filename;
	}
	
	public void process() {
		// 1. Odczytać wszystkie linie z pliku
		File f = new File(path + filename);
		double avg = 0;
		
		LinkedList<String> fileContent = new LinkedList<>();
		
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				if(!currentLine.equals("")) {
					fileContent.add(currentLine);
					avg += countAvgFromLine(currentLine);
				}
			}
			// 2. Obliczenie sredniej ze średnich poszczególnych uczniów
			//    suma srednich / ilosc linii
			avg /= fileContent.size(); // avg /= 4;   <=>   avg = avg / 4;
			sc.close();
			// 3. Iteracja po odczytanych linijach i umieszczenie w pliku
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			for(String line : fileContent) {
				//   tylko tych wpisów, których średnia się zgadza
				//	  czyli jest wieksza niż srednia srednich
				if(countAvgFromLine(line) > avg) {
					
					pw.println(line);
				}
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private double countAvgFromLine(String line) {
		// a. podczas odczytu aktualnej linii, mozemy rozbic ją po \t
		String[] marks = line.split("\t"); // [ "Pawel", "5", "4", "3" ]
		double marksSum = 0;
		// i poszczególne wartości sparsować na typ double
		for(int i = 1; i < marks.length; i++) {
			marksSum += Double.parseDouble(marks[i]);
		}
		return marksSum / (marks.length - 1);
	}
}
