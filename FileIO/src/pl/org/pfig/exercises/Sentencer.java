package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Sentencer extends Sentence{

	// +writeSentence(filename:String, sentence:String):void
	
	public void writeSentence(String filename, String sentence) {
		File f = new File("resources/" + filename);
		
		try {
			// Domyslnie FOS nadpisuje cały plik
			FileOutputStream fos = new FileOutputStream(f);
			// Jezeli chcemy dopisywac do pliku (tryb append), to musimy przekazac drugi parametr 
			// do konstruktora FOS np.
			// FileOutputStream fos = new FileOutputStream(f, true);
			
			PrintWriter pw = new PrintWriter(fos);
			
			pw.println(sentence);
			pw.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	}
}
