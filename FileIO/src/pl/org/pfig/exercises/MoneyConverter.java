package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {

	private final String filename = "resources/currency.txt";
	// -readCourse(currency:String):double
	
	private double readCourse(String currency) {
		File f = new File(filename);
		double ret = 0;
		
		try {
			String currentLine;
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t");
				String[] values = temp[0].split(" ");
				if(values[1].equalsIgnoreCase(currency)) {
					ret = Double.parseDouble(temp[1].replace(",", ".")) / Integer.parseInt(values[0]);
					break;
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return ret;
	}
	
	public double convert(double money, String currency) {
		return money / this.readCourse(currency);
	}
	
	public double convert(double money, String to, String from) {
		return (money * this.readCourse(from)) / this.readCourse(to);
	}
	
}
