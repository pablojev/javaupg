package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
	
	private final String path = "resources/";
	
	//-isProperLength(String arg, int len):boolean 

	private boolean isProperLength(String arg, int len) {
		return arg.length() > len;
	}

	//-readFile(String filename):String[] 
	
	private String[] readFile(String filename) {
		File f = new File(path + filename);
		String[] ret = new String[countLines(filename)];
		int i = 0;
		try(Scanner sc = new Scanner(f)) {
						
			while(sc.hasNextLine()) {
				ret[i++] = sc.nextLine();
			}
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return ret;
	}
	
	private int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try(Scanner sc = new Scanner(f)) {
			while(sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
	
	// -writeFile(String[] fileContent, int len):void 
	
	private void writeFile(String[] fileContent, int len) {
		try {
			FileOutputStream fos = new FileOutputStream(path + "words_" + len + ".txt");
			PrintWriter pw = new PrintWriter(fos);
			for(String line : fileContent) {
				if(isProperLength(line, len)) {
					pw.println(line);
				}
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//+make(String fileInput, int len):void
	
	public void make(String fileInput, int len) {
		String[] fileContent = readFile(fileInput);
		writeFile(fileContent, len);
	}
}
