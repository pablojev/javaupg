package pl.org.pfig.main;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import pl.org.pfig.exercises.*;

public class Main {

	public static void main(String[] args) {
		
		long heapSize = Runtime.getRuntime().totalMemory();
		
		Properties prop = System.getProperties();
		System.out.println ("JVM Vendor : " + prop.getProperty("java.vendor") );
		System.out.println ("JVM Version: " + prop.getProperty("java.version") );
		
		Runtime.getRuntime().gc();
		
		//Print the jvm heap size.
		System.out.println("Heap Size = " + humanReadableByteCount(heapSize, false));
		
		
		System.exit(0);
		
		
		Columner c = new Columner("data.txt");
		try {
			System.out.println(c.sumColumn(5));
		} catch (WrongColumnException e) {
			System.out.println(e.getMessage());
		}
//		AvgChecker avgc = new AvgChecker("marks.txt");
//		avgc.process();
		
//		University u = new University();
//		try {
//			System.out.println(u.getStudent(1));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		Student s1 = new Student(123, "Paweł", "Test", "programowanie", 4.12);
//		Student s2 = new Student(56, "Piotr", "Testowy", "garncarstwo", 3.32);
//		
//		HashMap<Integer, Student> students = new HashMap<>();
//		
//		students.put(s1.getIndex(), s1);
//		students.put(s2.getIndex(), s2);
//		
//		if(students.containsKey(56)) {
//			
//		}
		
//		LengthChecker lc = new LengthChecker();
//		
//		lc.make("words.txt", 8);
		
//		TempConverter tc = new TempConverter();
//		double[] tempC = tc.readTemp("tempC.txt");
//		
//		tc.writeTemp(tempC);
		
		//System.out.println(tc.countLines("tempC.txt"));
		
//		MoneyConverter mc = new MoneyConverter();
//		
//		List<String> currencies = Arrays.asList("USD", "EUR", "JPY", "KRW");
//		
//		for(String c : currencies) {
//			//System.out.println(c + " | " + mc.readCourse(c));
//		}
		
//		Sentencer s = new Sentencer();
//		
//		String mySentence = s.readSentence("test.txt");
//		
//		s.writeSentence("mySentence.txt", mySentence);
//		
//		
//		String myStr = "1 CNY	0,6064";
//		String[] temp = myStr.split("\t");
//		
//		// temp[0] = "1 CNY";
//		// temp[1] = "0,6064"
//		
//		String[] values = temp[0].split(" ");
//		
//		// values[0] = "1";
//		// values[1] = "CNY"
//		
//		
//		int howMany = Integer.parseInt(values[0]);
//		String currency = values[1].trim();
//		double course = Double.parseDouble(temp[1].replace(",", "."));
//		
//		System.out.println(howMany + " | " + currency + " | " + course);
	}
	
	public static String humanReadableByteCount(long bytes, boolean si) {
	    int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}


}
